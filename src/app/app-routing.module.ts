import { LogoutComponent } from './account/logout/logout.component';
import { AccountComponent } from './account/account.component';
import { LoginComponent } from './account/login/login.component';
import { UserDetailsComponent } from './admin/users/user-details/user-details.component';
import { UserCreateComponent } from './admin/users/user-create/user-create.component';
import { UsersListComponent } from './admin/users/users-list/users-list.component';
import { UsersComponent } from './admin/users/users.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { RegisterComponent } from './account/register/register.component';
import { ResetPasswordComponent } from './account/reset-password/reset-password.component';


const routes: Routes = [
  { path: 'admin', component: AdminComponent,
    children: [
      { path: 'users', component: UsersComponent,
        children: [
          { path: '', component: UsersListComponent },
          { path: 'create', component: UserCreateComponent },
          { path: ':user_id', component: UserDetailsComponent }
        ]
      },
    ]
  },
  {
    path: '', component: AccountComponent,
    children: [
      { path: '', redirectTo: 'account/login', pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'logout', component: LogoutComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'reset-password', component: ResetPasswordComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

